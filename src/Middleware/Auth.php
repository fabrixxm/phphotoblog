<?php

namespace PhPhotoBlog\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;


class Auth {
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function __get($property) {
        if (isset($this->container->{$property})) {
            return $this->container->{$property};
        }
        // error
    }

    public function __invoke(Request $request, Response $response, $next) {
        if (!isset($_SESSION['user']) || !isset($_SESSION['admin']) || $_SESSION['admin'] !== true) {
            // no logged user. go to login
            return $response->withRedirect($this->router->pathFor('login'));
        }
        return $next($request, $response);
    }
}
