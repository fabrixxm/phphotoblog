<?php
use Slim\Http\UploadedFile;

/**
 * Moves the uploaded file to the upload directory and assigns it a unique name
 * to avoid overwriting an existing uploaded file.
 *
 * @param string $uploadpath directory to which the file is moved
 * @param UploadedFile $uploaded file uploaded file to move
 * @return string filename of moved file
 */
function moveUploadedFile($uploadpath, UploadedFile $uploadedFile)
{
    $s = DIRECTORY_SEPARATOR;
    $subpath = date("Y{$s}m{$s}d");
    $path = $uploadpath . DIRECTORY_SEPARATOR . $subpath;
    mkdir($path, 0777, true);
    
    $filename = preg_replace("/[^a-zA-Z0-9_.-]/", "_", $uploadedFile->getClientFilename());
    $filename = $path . DIRECTORY_SEPARATOR . $filename;
    $uploadedFile->moveTo($filename);
    
    $filename = str_replace($uploadpath, "", $filename);
    return $filename;
}

/**
 * Create thumbnail file if doesn't exists
 *
 * @parm  string $filename  original image file
 * @param  int $thumb_size  thumb image size. Optional, default 200
 * @return  string  thumbnail image file
 */
function makeThumbnail($uploadpath, $fileName, $thumb_size = 200) {
    if ($fileName === '') return '';
    
    $thumbnail = dirname($fileName). "/thumb_" . basename($fileName);
    if (file_exists($thumbnail)) return $thumbnail;
    
    list($width, $height) = getimagesize($uploadpath . $fileName);
    $thumb_create = imagecreatetruecolor($thumb_size, $thumb_size);
    
    $srcsize = $width;
    $x = 0; $y = 0;
    if ($width > $height) { 
        $srcsize = $height;
        $x = ($width - $height) / 2;
    }
    if ($height > $height) {
        $srcsize = $width;
        $y = ($height - $width) / 2;
    }
    
    
    list($mime_type, $mime_subtype) = explode('/', mime_content_type($uploadpath . $fileName));
    switch($mime_subtype){
        case 'jpeg':
            $source = imagecreatefromjpeg($uploadpath . $fileName);
            break;
        case 'png':
            $source = imagecreatefrompng($uploadpath . $fileName);
            break;
        case 'gif':
            $source = imagecreatefromgif($uploadpath . $fileName);
            break;
        default:
            $source = imagecreatefromjpeg($uploadpath . $fileName);
    }

    imagecopyresized($thumb_create, $source, 0, 0, $x, $y, $thumb_size, $thumb_size, $srcsize, $srcsize);
    switch($mime_subtype){
        case 'jpeg':
            imagejpeg($thumb_create, $uploadpath . $thumbnail, 60);
            break;
        case 'png':
            imagepng($thumb_create, $uploadpath . $thumbnail, 9);
            break;
        case 'gif':
            imagegif($thumb_create, $uploadpath . $thumbnail);
            break;
        default:
            imagejpeg($thumb_create, $uploadpath . $thumbnail, 60);
    }


    return $thumbnail;
}

/**
 * Return the error message for file upload error code
 *
 * @param int $code  The error code
 * @return string  The error message
 */
function errorUploadedFile($code) {
    switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
            $message = "The uploaded file exceeds the upload max filesize of " . ini_get("upload_max_filesize") . " defined in php.ini";
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
            break;
        case UPLOAD_ERR_PARTIAL:
            $message = "The uploaded file was only partially uploaded";
            break;
        case UPLOAD_ERR_NO_FILE:
            $message = "No file was uploaded";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $message = "Missing a temporary folder";
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $message = "Failed to write file to disk";
            break;
        case UPLOAD_ERR_EXTENSION:
            $message = "File upload stopped by extension";
            break; 
        default:
            $message = "Unknown upload error";
            break;
    }
    return $message; 
}

