<?php
require 'fileuploadutils.php';

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Container;

use PhPhotoBlog\Model\Photo;
use PhPhotoBlog\Middleware\Auth;

// Public routes

/**
 * Home page: show last photo
 */
$app->get('/', function (Request $request, Response $response, array $args) {
    $photo = Photo::getLastPublic();
    
    // Render index view
    return $this->view->render($response, 'photo.html', [
        'photo' => $photo,
    ]);
});

/**
 * Show page for photo # id
 */
$app->get('/photo/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $photo = Photo::getPublicById($id);
    
    if ($photo === false) return ($this->notFoundHandler)($request, $response);
    
    return $this->view->render($response, 'photo.html', [
        'photo' => $photo,
    ]);
})->setName('photo');


/**
 * Show gallery
 */
$app->get('/photos', function (Request $request, Response $response, array $args) {
    $sth = $this->db->prepare('
        SELECT * FROM photo
            WHERE status = :status and published < :now
            ORDER BY published
    ');
    $sth->bindValue(':status', Photo::PUBLIC);
    $sth->bindValue(':now', time());
    $sth->execute();
    $photos = $sth->fetchAll(PDO::FETCH_CLASS, Photo::class, [$this]);
    
    return $this->view->render($response, 'gallery.html', [
        'photos' => $photos,
    ]);
})->setName('gallery');




// Admin routes

$app->get('/login', function (Request $request, Response $response, array $args) {
    return $this->view->render($response, 'admin/login.html', []);
})->setName('login');


$app->post('/login', function (Request $request, Response $response, array $args) {
    $user = trim($request->getParsedBodyParam('inputUser', ''));
    $pass = trim($request->getParsedBodyParam('inputPassword', ''));

    if ($user == '' || $pass == '' || $user !== getenv('ADMIN_USER') || $pass !== getenv('ADMIN_PASSWORD')) {
        $this->flash->addMessage('danger', 'Invalid credentials.');
        return $response->withRedirect($this->router->pathFor('login'));
    }

    $_SESSION['user'] = $user;
    $_SESSION['admin'] = true;
    
    $this->flash->addMessage('success', 'Logged in');
    return $response->withRedirect($this->router->pathFor('admin'));
})->setName('loginpost');

$app->get('/logout', function (Request $request, Response $response, array $args) {
    unset($_SESSION['user']);
    unset($_SESSION['admin']);

    $this->flash->addMessage('success', 'Logged out');
    return $response->withRedirect($this->router->pathFor('login'));
})->setName('logout');

$app->group('/admin', function(App $app) {

    $app->get('', function (Request $request, Response $response, array $args) {
        $sth = $this->db->prepare('SELECT * FROM photo ORDER BY created DESC');
        $sth->execute();
        $photos = $sth->fetchAll(PDO::FETCH_CLASS, Photo::class, [$this]);

        return $this->view->render($response, 'admin/index.html', [
            'photos' => $photos,
        ]);
    })->setName('admin');


    $app->get('/photo/new', function(Request $request, Response $response, array $args) {
        return $this->view->render($response, 'admin/edit.html', [
            'photo' => new Photo($this),
        ]);
    })->setName('create');



    $app->post('/photo/new', function(Request $request, Response $response, array $args) {
        $photo = new Photo($this);
        
        return photoFormSave($photo, 'create', $request, $response, $this);
    })->setName('createpost');



    $app->get('/photo/{id:[0-9]+}', function(Request $request, Response $response, array $args) {
        $id = $args['id'];
        $photo = Photo::getById($id);
        
        if ($photo === false) return ($this->notFoundHandler)($request, $response);
        
        return $this->view->render($response, 'admin/edit.html', [
            'photo' => $photo,
        ]);
    })->setName('edit');


    $app->post('/photo/{id:[0-9]+}', function(Request $request, Response $response, array $args) {
        $id = $args['id'];
        $photo = Photo::getById($id);
        
        return photoFormSave($photo, 'edit', $request, $response, $this);
    })->setName('editpost');


    $app->post('/photo/{id:[0-9]+}/delete', function(Request $request, Response $response, array $args) {
        $id = $args['id'];
        $photo = Photo::getById($id);
        if ($photo === false) return ($this->notFoundHandler)($request, $response);
        
        if ($photo->delete()) {
            $this->flash->addMessage('success', 'Photo #' . $id . ' deleted');
        } else {
            $this->flash->addMessage('danger', 'Error deleting ' . $photo);
        }
        $url = $this->router->pathFor('admin');
        return $response->withRedirect($url);
    })->setName('delete');
})->add( new Auth($app->getContainer()) );



/**
 * Create tables
 */
$app->get('/install', function(Request $request, Response $response) {
    $this->db->exec('
         CREATE TABLE IF NOT EXISTS photo (
            id INTEGER PRIMARY KEY,
            title TEXT,
            description TEXT,
            exif TEXT,
            file TEXT,
            fileorig TEXT,
            created INTEGER,
            published INTEGER,
            modified INTEGER,
            status INTEGER
        );

        CREATE TABLE IF NOT EXISTS tag (
            name TEXT PRIMARY KEY
        ) WITHOUT ROWID;

        CREATE TABLE IF NOT EXISTS tag_post (
            tag TEXT REFERENCES tag (name) ON DELETE CASCADE ON UPDATE CASCADE,
            post INTEGER REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
        );
    ');

    return $this->view->render($response, 'install.html', [
        'messages' => $this->db->errorInfo(),
    ]);
});



/**
 * Save data from photo edit form
 */
function photoFormSave($photo, $action, Request $request, Response $response, Container $container) {
    $photo->title = trim($request->getParsedBodyParam('title', $photo->title));
    $photo->description = trim($request->getParsedBodyParam('description', $photo->description));
    $photo->exif = trim($request->getParsedBodyParam('exif', $photo->exif));
    $photo->status = trim($request->getParsedBodyParam('status', $photo->status));

    $file_delete = $request->getParsedBodyParam('delete_file', false) != false;
    $fileorig_delete = $request->getParsedBodyParam('delete_fileorig', false) != false;

    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles['file'];
    $uploadedFileOrig = $uploadedFiles['fileorig'];
    
    
    $errors = [];
    if ($photo->title === '') {
        $errors['title'] = 'Title is required';
    }
    
    if ($photo->file === '') {
        if ($uploadedFile->getError() === UPLOAD_ERR_NO_FILE) {
            $errors['file'] = 'Photo file is required';
        } else if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
            $errors['file'] = errorUploadedFile($uploadedFile->getError());
        }
    } else {
         if ($uploadedFile->getError() !== UPLOAD_ERR_OK && $uploadedFile->getError() !== UPLOAD_ERR_NO_FILE) {
            $errors['file'] = errorUploadedFile($uploadedFile->getError());
        }
    }
    
    if ($uploadedFileOrig->getError() !== UPLOAD_ERR_OK && $uploadedFileOrig->getError() !== UPLOAD_ERR_NO_FILE) {
        $errors['fileorig'] = errorUploadedFile($uploadedFileOrig->getError());
    }

    if (count($errors) == 0) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $photo->file = moveUploadedFile($container->settings['upload']['path'], $uploadedFile);
        }
        
        if ($fileorig_delete === false && $uploadedFileOrig->getError() === UPLOAD_ERR_OK) {
            $photo->fileorig = moveUploadedFile($container->settings['upload']['path'], $uploadedFileOrig);
        }
        if ($fileorig_delete === true) {
            /// @TODO Delete file from disk
            $photo->fileorig = '';
        }
        
        if (function_exists('exif_read_data') && $photo->exif == '') {
            $exif = exif_read_data($container->settings['upload']['path'] . $photo->file, 0, true);
            if ($exif === false) {
                $photo->exif = '{}';
            } else {
                $photo->exif = json_encode($exif);
            }
        }
        
        /// @TODO: strip exif tags
        // https://stackoverflow.com/questions/3614925/remove-exif-data-from-jpg-using-php
        
        if ($photo->save()) {
            switch ($action) {
                case 'create':
                    $container->flash->addMessage('success',  $photo . ' created');
                    break;
                case 'edit':
                    $container->flash->addMessage('success',  $photo . ' updated');
                    break;
            }
            $url = $container->router->pathFor('admin');
            return $response->withRedirect($url);
        } else {
            $container->flash->addMessage('danger', 'Error saving ' . $photo);
        }
        $url = $container->router->pathFor('admin');
        return $response->withRedirect($url);
    }

    return $container->view->render($response, 'admin/edit.html', [
        'photo' => $photo,
        'errors' => $errors,
    ]);
}

