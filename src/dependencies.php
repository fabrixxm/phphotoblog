<?php
// DIC configuration

$container = $app->getContainer();

// twig view renderer
$container['view'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    
    $view = new \Slim\Views\Twig($settings['template_path'], [
        'cache' => $settings['cache_path'],
        'debug' => true,
    ]);

    $view->addExtension(new Twig_Extension_Debug());
    $view->addExtension(new Knlv\Slim\Views\TwigMessages($c->flash));

    // Instantiate and add Slim specific extension
    $router = $c->get('router');

    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));

    return $view;
};

// database
$container['db'] = function($c) {
    $settings = $c->get('settings')['database'];

    $db = new PDO('sqlite:' . $settings['path']);
    $db->exec('PRAGMA foreign_keys = ON;');
    
    return $db;
};

// flash messages
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

