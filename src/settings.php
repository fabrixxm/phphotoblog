<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => dirname(__DIR__) . '/templates/',
            'cache_path' => false, //dirname(__DIR__) . '/cache/',
        ],
        
        'database' => [
            'path' => dirname(__DIR__) . '/database/db.sqlite3',
        ],
        
        'upload' => [
            'path' => dirname(__DIR__) . '/public/upload',
            'url' => '/upload',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'phphotoblog',
            'path' => dirname( __DIR__) . '/logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
