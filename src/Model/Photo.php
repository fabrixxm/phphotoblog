<?php
namespace PhPhotoBlog\Model;

class Photo {
    const DRAFT = 0;
    const PUBLIC = 1;

    var $id = null;
    var $title = '';
    var $description = '';
    var $exif = '';
    var $file = '';
    var $fileorig = '';
    var $created = 0;
    var $modified = 0;
    var $published = 0;
    var $status = Photo::DRAFT;
    
    var $tags = [];
    
    private $db;
    private $settings;
    
    private $nextid = null;
    private $previd = null;
    
    public function __construct($container) {
        $this->settings = $container->settings;
        $this->db = $container->db;
    }
    
    /**
     * Get photo file public url
     *
     * @return string
     */
    public function photoUrl() {
        return $this->settings['upload']['url'] . $this->file; 
    }
    
    
    /**
     * Get original photo file public url
     *
     * @return string
     */
    public function originalPhotoUrl() {
        return $this->settings['upload']['url'] . $this->fileorig; 
    }
    
    
    /**
     * Get thumbnail file public url
     *
     * @return string
     */
    public function photoThumbUrl() {
        return $this->settings['upload']['url'] . makeThumbnail($this->settings['upload']['path'], $this->file);
    }
    
    
    /**
     * Get the id of the next public photo
     *
     * @return int|boolean FALSE if no next photo
     */
    public function nextPublicId() {
        if (is_null($this->nextid)) {
            $sth = $this->db->prepare('
                SELECT id FROM photo
                WHERE status = :status and published > :thisphoto and published < :now
                ORDER BY published ASC LIMIT 1
            ');
            $sth->bindValue(':status', Photo::PUBLIC, \PDO::PARAM_INT);
            $sth->bindValue(':thisphoto', $this->published, \PDO::PARAM_INT);
            $sth->bindValue(':now', time(), \PDO::PARAM_INT);
            $r = $sth->execute();
            
            $this->nextid = $sth->fetchColumn(0);
        }
        
        return $this->nextid;
    }

    /**
     * Get the id of the previous public photo
     *
     * @return int|boolean FALSE if no next photo
     */
    public function prevPublicId() {
        if (is_null($this->previd)) {
            $sth = $this->db->prepare('
                SELECT id FROM photo
                WHERE status = :status and published < :thisphoto and published < :now
                ORDER BY published DESC LIMIT 1
            ');
            $sth->bindValue(':status', Photo::PUBLIC, \PDO::PARAM_INT);
            $sth->bindValue(':thisphoto', $this->published, \PDO::PARAM_INT);
            $sth->bindValue(':now', time(), \PDO::PARAM_INT);
            $r = $sth->execute();
            
            $this->previd = $sth->fetchColumn(0);
        }
        
        return $this->previd;
    }

    /**
     * String rapresentation
     */
    public function __toString() {
        if ($this->title !== "") return $this->title;
        if (is_null($this->id)) {
            return "New Photo";
        }
        return "Photo #" . $this->id;
    }
    
    
    /**
     * Save current photo object in database
     *
     * @return bool True if object was saved succesfully
     */
    public function save() {
        $r = false;
        $this->modified = time();
        
        if ($this->status == Photo::PUBLIC && $this->published == 0) {
            $this->published = time();
        }
        
        if (is_null($this->id)) {
            // INSERT
            $this->created = time();
            
            $sth = $this->db->prepare('
                INSERT INTO photo (title, description, exif, file, fileorig, created, published, modified, status)
                VALUES (:title, :description, :exif, :file, :fileorig, :created, :published, :modified, :status);
            ');
            if ($sth === false) {
                print_r($this->db->errorInfo());
                die();
            }
            $sth->bindParam(':title', $this->title);
            $sth->bindParam(':description', $this->description);
            $sth->bindParam(':exif', $this->exif);
            $sth->bindParam(':file', $this->file);
            $sth->bindParam(':fileorig', $this->fileorig);
            $sth->bindParam(':created', $this->created, \PDO::PARAM_INT);
            $sth->bindParam(':published', $this->published, \PDO::PARAM_INT);
            $sth->bindParam(':modified', $this->modified, \PDO::PARAM_INT);
            $sth->bindParam(':status', $this->status, \PDO::PARAM_INT);
            
            $r = $sth->execute();
            if ($r) {
                $this->id = $this->db->lastInsertId();
            }
        } else {
            // UPDATE
            $sth = $this->db->prepare('
                UPDATE photo SET title=:title, description=:description, exif=:exif, file=:file, fileorig=:fileorig, 
                created=:created, published=:published, modified=:modified, status=:status
                WHERE id=:id;
            ');
            if ($sth === false) {
                print_r($this->db->errorInfo());
                die();
            }
            $sth->bindParam(':id', $this->id, \PDO::PARAM_INT);
            $sth->bindParam(':title', $this->title);
            $sth->bindParam(':description', $this->description);
            $sth->bindParam(':exif', $this->exif);
            $sth->bindParam(':file', $this->file);
            $sth->bindParam(':fileorig', $this->fileorig);
            $sth->bindParam(':created', $this->created, \PDO::PARAM_INT);
            $sth->bindParam(':published', $this->published, \PDO::PARAM_INT);
            $sth->bindParam(':modified', $this->modified, \PDO::PARAM_INT);
            $sth->bindParam(':status', $this->status, \PDO::PARAM_INT);
            
            $r = $sth->execute();
        }
    
        return $r;
    }
    
    /**
     * Delete photo object from database
     *
     * @TODO Delete uploaded files
     * @return boolean
     */
    public function delete() {
        if (is_null($this->id)) return true;
        
        $sth = $this->db->prepare('DELETE FROM photo WHERE id = :id');
        $sth->bindParam(':id', $this->id);
        return $sth->execute();
    }
    
    // STATIC FUNCTIONS
    
    private static function container() {
        global $app;
        return $app->getContainer();
    }
    
    /**
     * Fetch a photo object from database by photo id
     *
     * @param  int $id  The photo id
     * @return Photo  or FALSE on errors
     */
    public static function getById($id) {
        $c = self::container();
        
        $sth = $c->db->prepare('SELECT * FROM photo WHERE id = :id');
        $sth->bindValue(':id', $id);
        $r = $sth->execute();
        return $sth->fetchObject(self::class, [$c]);
    }

    /**
     * Fetch a public photo object from database by photo id
     *
     * @param  int $id  The photo id
     * @return Photo  or FALSE on errors
     */
    public static function getPublicById($id) {
        $c = self::container();
        
        $sth = $c->db->prepare('
            SELECT * FROM photo 
            WHERE id = :id and status = :status and published > 0 and published < :now
        ');
        $sth->bindValue(':id', $id, \PDO::PARAM_INT);
        $sth->bindValue(':status', Photo::PUBLIC);
        $sth->bindValue(':now', time(), \PDO::PARAM_INT);
        
        
        
        $r = $sth->execute();
        return $sth->fetchObject(self::class, [$c]);
    }

    
    /**
     * Fetch last public photo from database
     *
     * @return Photo  or FALSE on errors
     */
    public static function getLastPublic() {
        $c = self::container();
        $sth = $c->db->prepare('
            SELECT * FROM photo 
            WHERE status = :status and published > 0 and published < :now
            ORDER BY published DESC LIMIT 1
        ');
        $sth->bindValue(':status', Photo::PUBLIC);
        $sth->bindValue(':now', time(), \PDO::PARAM_INT);
        
        $r = $sth->execute();
        return $sth->fetchObject(self::class, [$c]);
    }
}

