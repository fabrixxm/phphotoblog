
/* image widget */
$('.photo-upload-file').on('change', function(evt) {
    var url=URL.createObjectURL(evt.target.files[0]);
    $(this).siblings('img')[0].src = url;
    var $del = $(this).siblings('.photo-upload-delete');
    $del.prop('checked', false);
    update_delete($del);
});
$('.photo-upload-delete').on('change', function(evt) {
    update_delete($(this));
});

function update_delete($elm) {
    if ($elm.prop('checked')) {
        $elm.parent().find('.photo-upload-delete-label')
                .removeClass("btn-outline-warning")
                .addClass("btn-danger");
        $elm.siblings('img')[0].src = "/placeholder.svg";
    } else {
        $elm.parent().find('.photo-upload-delete-label')
                .addClass("btn-outline-warning")
                .removeClass("btn-danger");
    }
}
