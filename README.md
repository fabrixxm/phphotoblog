# PhPhotoBlog

A simple, single-user, photo blog.

Features:

* Simple and clean public interface
* Simple admin interface
* Create a draft, publish it
* Set a future publication date (planned)
* Each photo can have title, a description
* Set tags (planned)
* Read exif data and store in db. 
* Select which data to publish, if any (planned)
* Strip exif data from the photo (planned)
* Set title, description and tags from exif (planned)


## Install the Application

Run this command from the directory in which you want to install PhPhotoBlog:

    php composer.phar install

* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/`, `database/` and `public/upload/` folders are writeable by php.

Copy `env.dist` to `.evn` and set administrator username and password

Go to url

    http://yourserver.com/install

to create the database.

That's it! Now go photograph something cool!

## Development

To run the application in development, you can run these commands 

    php composer.phar start

Run this command in the application directory to run the test suite

    php composer.phar test


